<?php

namespace Drupal\jobsubmission\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "job_submission_block",
 *   admin_label = @Translation("Job Submission Block"),
 * )
 */
class JobSubmissionBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
      return \Drupal::formBuilder()->getForm('Drupal\jobform\Form\JobForm');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['job_submission_block_settings'] = $form_state->getValue('job_submission_block_settings');
  }
}