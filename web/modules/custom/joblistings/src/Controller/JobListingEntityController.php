<?php

namespace Drupal\joblistings\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Drupal\joblistings\Entity\JobListingEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class JobListingEntityController.
 *
 *  Returns responses for Job Listing routes.
 */
class JobListingEntityController extends ControllerBase implements ContainerInjectionInterface {


  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a new JobListingEntityController.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(DateFormatter $date_formatter, Renderer $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Displays a Job Listing revision.
   *
   * @param int $joblisting_entity_revision
   *   The Job Listing revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($joblisting_entity_revision) {
    $joblisting_entity = $this->entityTypeManager()->getStorage('joblisting_entity')
      ->loadRevision($joblisting_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('joblisting_entity');

    return $view_builder->view($joblisting_entity);
  }

  /**
   * Page title callback for a Job Listing revision.
   *
   * @param int $joblisting_entity_revision
   *   The Job Listing revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($joblisting_entity_revision) {
    $joblisting_entity = $this->entityTypeManager()->getStorage('joblisting_entity')
      ->loadRevision($joblisting_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $joblisting_entity->label(),
      '%date' => $this->dateFormatter->format($joblisting_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Job Listing.
   *
   * @param \Drupal\joblistings\Entity\JobListingEntityInterface $joblisting_entity
   *   A Job Listing object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(JobListingEntityInterface $joblisting_entity) {
    $account = $this->currentUser();
    $joblisting_entity_storage = $this->entityTypeManager()->getStorage('joblisting_entity');

    $langcode = $joblisting_entity->language()->getId();
    $langname = $joblisting_entity->language()->getName();
    $languages = $joblisting_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $joblisting_entity->label()]) : $this->t('Revisions for %title', ['%title' => $joblisting_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all job listing revisions") || $account->hasPermission('administer job listing entities')));
    $delete_permission = (($account->hasPermission("delete all job listing revisions") || $account->hasPermission('administer job listing entities')));

    $rows = [];

    $vids = $joblisting_entity_storage->revisionIds($joblisting_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\joblistings\JobListingEntityInterface $revision */
      $revision = $joblisting_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $joblisting_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.joblisting_entity.revision', [
            'joblisting_entity' => $joblisting_entity->id(),
            'joblisting_entity_revision' => $vid,
          ]));
        }
        else {
          $link = $joblisting_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.joblisting_entity.translation_revert', [
                'joblisting_entity' => $joblisting_entity->id(),
                'joblisting_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.joblisting_entity.revision_revert', [
                'joblisting_entity' => $joblisting_entity->id(),
                'joblisting_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.joblisting_entity.revision_delete', [
                'joblisting_entity' => $joblisting_entity->id(),
                'joblisting_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['joblisting_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

  /**
   * View a Job Listing entity.
   *
   * @param \Drupal\joblistings\Entity\JobListingEntityInterface $joblisting_entity
   *   A Job Listing object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function content(JobListingEntityInterface $joblisting_entity) {
    return [
      '#markup' => 'cheese slice',
    ];
  }
}
