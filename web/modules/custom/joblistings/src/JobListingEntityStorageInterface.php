<?php

namespace Drupal\joblistings;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\joblistings\Entity\JobListingEntityInterface;

/**
 * Defines the storage handler class for Job Listing entities.
 *
 * This extends the base storage class, adding required special handling for
 * Job Listing entities.
 *
 * @ingroup joblistings
 */
interface JobListingEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Job Listing revision IDs for a specific Job Listing.
   *
   * @param \Drupal\joblistings\Entity\JobListingEntityInterface $entity
   *   The Job Listing entity.
   *
   * @return int[]
   *   Job Listing revision IDs (in ascending order).
   */
  public function revisionIds(JobListingEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Job Listing author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Job Listing revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\joblistings\Entity\JobListingEntityInterface $entity
   *   The Job Listing entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(JobListingEntityInterface $entity);

  /**
   * Unsets the language for all Job Listing with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
