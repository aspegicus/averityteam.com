<?php

namespace Drupal\joblistings\Entity;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Job Listing entity.
 *
 * @ingroup joblistings
 *
 * @ContentEntityType(
 *   id = "joblisting_entity",
 *   label = @Translation("Job Listing"),
 *   handlers = {
 *     "storage" = "Drupal\joblistings\JobListingEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\joblistings\JobListingEntityListBuilder",
 *     "views_data" = "Drupal\joblistings\Entity\JobListingEntityViewsData",
 *     "translation" = "Drupal\joblistings\JobListingEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\joblistings\Form\JobListingEntityForm",
 *       "add" = "Drupal\joblistings\Form\JobListingEntityForm",
 *       "edit" = "Drupal\joblistings\Form\JobListingEntityForm",
 *       "delete" = "Drupal\joblistings\Form\JobListingEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\joblistings\JobListingEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\joblistings\JobListingEntityAccessControlHandler",
 *   },
 *   base_table = "joblisting_entity",
 *   data_table = "joblisting_entity_field_data",
 *   revision_table = "joblisting_entity_revision",
 *   revision_data_table = "joblisting_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer job listing entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/joblisting/{joblisting_entity}",
 *     "add-form" = "/admin/structure/joblisting_entity/add",
 *     "edit-form" = "/admin/structure/joblisting_entity/{joblisting_entity}/edit",
 *     "delete-form" = "/admin/structure/joblisting_entity/{joblisting_entity}/delete",
 *     "version-history" = "/admin/structure/joblisting_entity/{joblisting_entity}/revisions",
 *     "revision" = "/admin/structure/joblisting_entity/{joblisting_entity}/revisions/{joblisting_entity_revision}/view",
 *     "revision_revert" = "/admin/structure/joblisting_entity/{joblisting_entity}/revisions/{joblisting_entity_revision}/revert",
 *     "revision_delete" = "/admin/structure/joblisting_entity/{joblisting_entity}/revisions/{joblisting_entity_revision}/delete",
 *     "translation_revert" = "/admin/structure/joblisting_entity/{joblisting_entity}/revisions/{joblisting_entity_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/joblisting_entity",
 *   },
 *   field_ui_base_route = "joblisting_entity.settings"
 * )
 */
class JobListingEntity extends RevisionableContentEntityBase implements JobListingEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the joblisting_entity owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * Custom getters and setters for the Account entity.
   */

  /**
   * {@inheritdoc}
   */
  public function getCrelateId() {
    return $this->get('crelate_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getModifiedDate() {
    return $this->get('modified_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setModifiedDate($timestamp) {
    $this->set('modified_date', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPostedDate() {
    return $this->get('posted_date')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPostedDate($timestamp) {
    $this->set('posted_date', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobLocationState()
  {
    return $this->get('job_location_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setJobLocationState($job_location_state) {
    $this->set('job_location_state', $job_location_state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobLocationCity()
  {
    return $this->get('job_location_city')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setJobLocationCity($job_location_city) {
    $this->set('job_location_city', $job_location_city);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobDescription()
  {
    return $this->get('job_description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setJobDescription($job_description) {
    $this->set('job_description', $job_description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobCompensation()
  {
    return $this->get('job_compensation')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setJobCompensation($job_compensation) {
    $this->set('job_compensation', $job_compensation);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobCode()
  {
    return $this->get('job_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobOwner()
  {
    return $this->get('job_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setJobOwner($job_owner) {
    $this->set('job_owner', $job_owner);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobStatus()
  {
    return $this->get('job_status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setJobStatus($job_status) {
    $this->set('job_status', $job_status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIsJobFeatured() {
    return $this->get('is_job_featured')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setIsJobFeatured($is_job_featured) {
    $this->set('is_job_featured', $is_job_featured ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getJobTags() {
    return $this->get('job_tags')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setJobTags($job_tag) {
    $field_items = $this->get('job_tags');

    $exists = FALSE;
    foreach ($field_items as $field_item) {
      if ($field_item->target_id === $job_tag->id()) {
        $exists = TRUE;
      }
    }

    if (!$exists) {
      $field_items->appendItem($job_tag);
    }

    return $this->set('job_tags', $field_items);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Job Listing entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Job Listing entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 120,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Job Listing is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    /**
     * Adding custom fields to the Account entity
     * @todo add getters above for all added custom fields below
     */

    /**
     * Crelate ID is a unique identifier for the Crelate database. It is also used to form url targeting the specific resource on crelate.com
     * e.g: https://app.crelate.com/go#stage/_Accounts/DefaultView/89d3b52a-40e9-47bc-9872-a78a012e0ff8
     * where the ID is the Crelate ID for that account (VICE).
     */
    $fields['crelate_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Crelate ID'))
      ->setDescription(t('Unique identifier coming from crelate.com'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'settings' => array(
          'display_label' => FALSE,
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['is_job_featured'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Featured'))
      ->setDescription(t('Indicates whether this Job Listing is featured on the job board home page.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['posted_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Posted Date'))
      ->setDescription(t('The posted date for this job listing.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'datetime_type' => 'date'
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'medium',
        ],
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['modified_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Modified Date'))
      ->setDescription(t('The last modified date for this job listing.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'datetime_type' => 'date'
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'datetime_default',
        'settings' => [
          'format_type' => 'medium',
        ],
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['job_description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Job Description'))
      ->setDescription(t('Job Description'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['job_location_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('Job Location State'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'settings' => array(
          'display_label' => FALSE,
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setRequired(FALSE);

    $fields['job_location_city'] = BaseFieldDefinition::create('string')
      ->setLabel(t('City'))
      ->setDescription(t('Job Location City'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'settings' => array(
          'display_label' => FALSE,
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setRequired(FALSE);

    $fields['job_compensation'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Compensation'))
      ->setDescription(t('Job Salary/Range'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'settings' => array(
          'display_label' => FALSE,
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setRequired(FALSE);

    $fields['job_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Application Job Code'))
      ->setDescription(t('Crelate Job Code used to generate the application email.'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'settings' => array(
          'display_label' => FALSE,
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setRequired(FALSE)
      ->setReadOnly(TRUE);

    $fields['job_owner'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Job Owner'))
      ->setDescription(t('Job Owner at Averity'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'settings' => array(
          'display_label' => FALSE,
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setRequired(FALSE);

    $fields['job_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Job Status'))
      ->setDescription(t('Job Status in Crelate'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'settings' => array(
          'display_label' => FALSE,
        ),
      ))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setRequired(FALSE);

    $fields['job_tags'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Job Tags'))
      ->setDescription(t('Tags related to the Job Listing'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['tags' => 'tags']])
      // We want unlimited values for this field.
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'tags',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
/*
    $fields['company_ref'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Company'))
      ->setDescription(t('Company owning the Job Listing'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['industry' => 'industry']])
      // We want unlimited values for this field.
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label'  => 'hidden',
        'type'   => 'industry',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'industry',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
*/



    return $fields;
  }

}
