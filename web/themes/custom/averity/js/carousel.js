(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.averityCarousel = {
    attach: function(context) {
      $('.carousel-outer .field--name-field-carousel-slide').once().slick({
      	dots: true
      });
    },
  }

})(jQuery, Drupal);
