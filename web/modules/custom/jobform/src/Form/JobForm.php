<?php

namespace Drupal\jobform\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class JobForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'job_application_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Please enter your information and attach your up-to-date resume.'),
    ];


    $form['first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Name'),
      '#description' => $this->t('Enter your first Name.'),
      '#required' => TRUE,
    ];

    $form['last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last Name'),
      '#description' => $this->t('Enter your last Name.'),
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Enter your email.'),
      '#required' => TRUE,
    ];

    $form['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#description' => $this->t('Enter your phone number.'),
      '#required' => FALSE,
    ];

    $form ['#attributes'] = array(
      'enctype' => 'multipart/form-data'
    );

    $form['file_upload_details'] = array(
      '#markup' => t('<label for=edit-attach-resume-upload>Attach your resume</label>'),
    );

    $validators = array(
      'file_validate_extensions' => array('pdf doc docx'),
    );

    $form['attach_resume'] = array(
      '#type' => 'managed_file',
      '#name' => 'resume',
      '#title' => null,
      '#size' => 20,
      '#description' => t('PDF / DOC / DOCX formats only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://resume_submitted/',
      '#required' => TRUE,
    );


    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;

  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $first_name = $form_state->getValue('first_name');
    $last_name = $form_state->getValue('last_name');
    $email = $form_state->getValue('email');
    $resume = $form_state->getValue('attach_resume');

    if (!isset($first_name)) {
      // Set an error for the form element with a key of "first_name".
      $form_state->setErrorByName('first_name', $this->t('Your first name is required.'));
    }

    if (!isset($last_name)) {
      // Set an error for the form element with a key of "first_name".
      $form_state->setErrorByName('last_name', $this->t('Your last name is required.'));
    }

    if (!isset($email)) {
      // Set an error for the form element with a key of "email".
      $form_state->setErrorByName('email', $this->t('Your email is required.'));
    }

    if (!isset($resume)) {
      // Set an error for the form element with a key of "resume".
      $form_state->setErrorByName('attach_resume', $this->t('Your resume is required.'));
    }

  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();

    // Load the current Job Entity
    if ($entity = \Drupal::request()->attributes->get('joblisting_entity')) {
      // Great! We have access to the Job Listing entity
      $jobcode = $entity->getJobCode();

      // Preparation of emailing Crelate
      $mailManager = \Drupal::service('plugin.manager.mail');
      $langcode = \Drupal::currentUser()->getPreferredLangcode();

      // Get user email address
      $from_email = $form['email']['#value'];
      $params['message']['from'] = $from_email;
      // Setting body
      $params['message']['body'] = 'FirstName='.$form['first_name']['#value'].'&'.
                                      'LastName=' .$form['last_name']['#value'].'&'.
                                      'Email='.$from_email.'&'.
                                      'Phone='.$form['phone']['#value'];

      // Let's attach the file to the message
      // We lookup the file ID of the attached resume
      $fid = key($form['attach_resume']['#files']);
      $resume_file = \Drupal\file\Entity\File::load($fid);
      // Cleanup the filepath
      $raw_filepath = rawurldecode(parse_url($resume_file->url(), PHP_URL_PATH));
      $filepath = substr($raw_filepath, 1);
      // Finally!
      $attachment = [
        'filepath' => $filepath,
        'filename' => $resume_file->getFilename(),
        'filemime' => $resume_file->getMimeType(),
        ];
      $params['attachments'][] = $attachment;


      // We can get the url and load the entity that way?
      $to = $jobcode . '@crelate.net';
      $result = $mailManager->mail('jobform', 'application_submit', $to, $langcode, $params);

      if ($result['result'] !== true) {
        $messenger->addMessage('There was a problem sending your message and it was not sent.');
      } else {
        $messenger->addMessage('You successfully submitted your application for this job!');
      }
      // Redirect to home
      //$form_state->setRedirect('/our-jobs');
    }
    // We couldn't load the Job Listing entity so let's report that
    else {
      $messenger->addMessage('Error encountered! You Job submission was not successful.','ERROR');
    }
  }
}
