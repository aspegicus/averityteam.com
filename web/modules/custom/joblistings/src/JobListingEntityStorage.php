<?php

namespace Drupal\joblistings;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\joblistings\Entity\JobListingEntityInterface;

/**
 * Defines the storage handler class for Job Listing entities.
 *
 * This extends the base storage class, adding required special handling for
 * Job Listing entities.
 *
 * @ingroup joblistings
 */
class JobListingEntityStorage extends SqlContentEntityStorage implements JobListingEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(JobListingEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {joblisting_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {joblisting_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(JobListingEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {joblisting_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('joblisting_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
