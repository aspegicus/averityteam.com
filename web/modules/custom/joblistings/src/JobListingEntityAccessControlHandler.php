<?php

namespace Drupal\joblistings;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Job Listing entity.
 *
 * @see \Drupal\joblistings\Entity\JobListingEntity.
 */
class JobListingEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\joblistings\Entity\JobListingEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished job listing entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published job listing entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit job listing entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete job listing entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add job listing entities');
  }

}
