<?php

namespace Drupal\joblistings;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for joblisting_entity.
 */
class JobListingEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
