<?php

namespace Drupal\joblistings\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Job Listing entities.
 */
class JobListingEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
