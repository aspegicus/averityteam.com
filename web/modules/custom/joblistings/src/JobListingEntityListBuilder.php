<?php

namespace Drupal\joblistings;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\taxonomy\Entity\Term;

/**
 * Defines a class to build a listing of Job Listing entities.
 *
 * @ingroup joblistings
 */
class JobListingEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Title');
    //$header['job_tags'] = $this->t('Tags');
    //$header['job_description'] = $this->t('Description');
    $header['job_location_city'] = $this->t('City');
    $header['job_location_state'] = $this->t('State');
    //$header['modified_date'] = $this->t('Modified');
    //$header['posted_date'] = $this->t('Posted');
    $header['job_compensation'] = $this->t('Compensation');
    $header['job_code'] = $this->t('Job Code');
    $header['job_owner'] = $this->t('Owner');
    $header['job_status'] = $this->t('Status');
    //$header['is_job_featured'] = $this->t('Featured');
    //$header['job_tags'] = $this->t('Tags');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\joblistings\Entity\JobListingEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.joblisting_entity.edit_form',
      ['joblisting_entity' => $entity->id()]
    );

    //@todo: Add all custom entity fields defined in JobListingEntity Class
    //$row['job_description'] = $entity->getJobDescription();
    $row['job_location_city'] = $entity->getJobLocationCity();
    $row['job_location_state'] = $entity->getJobLocationState();
    //$row['modified_date'] = $entity->getModifiedDate()->date->format('Y-m-d H:i:s');
    //$row['posted_date'] = $entity->getPostedDate()->date->format('Y-m-d H:i:s');
    $row['job_compensation'] = $entity->getJobCompensation();
    $row['job_code'] = $entity->getJobCode();
    $row['job_owner'] = $entity->getJobOwner();
    $row['job_status'] = $entity->getJobStatus();
    //$row['is_job_featured'] = $entity->getIsJobFeatured();
    /**
     * Job Tags
     */
    /*
    $tags = "";
    $all_tags = $entity->getJobTags();
    // Get array keys
    $arrayKeys = array_keys($all_tags);
    // Fetch last array key
    $lastArrayKey = array_pop($arrayKeys);
    foreach($all_tags as $tk => $tags) {
      $term = Term::load($tags->id());
      $tags .= $term->getName();
      // We append a coma after each term but the last
      if($tk != $lastArrayKey) {
        $all_tags .= ', ';
      }
    }
    $row['job_tags'] = $tags;
    */

    return $row + parent::buildRow($entity);
  }

}
