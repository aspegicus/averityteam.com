<?php

/**
 * @file
 * Contains joblisting_entity.page.inc.
 *
 * Page callback for Job Listing entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Job Listing templates.
 *
 * Default template: joblisting.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_joblisting_entity(array &$variables) {
  // Fetch JobListingEntity Entity Object.
  $joblisting_entity = $variables['elements']['#joblisting_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  /**
   * To be customized for templating needs
   */

  /** @var \Drupal\joblistings\Entity\JobListingEntityInterface $entity */
  $entity = $variables['elements']['#joblisting_entity'];
  //provide the label
  $variables['label'] = $entity->label();
  //provide the alias
  $variables['url'] = $entity->toUrl()->toString();
}
