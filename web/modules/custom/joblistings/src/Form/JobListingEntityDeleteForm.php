<?php

namespace Drupal\joblistings\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Job Listing entities.
 *
 * @ingroup joblistings
 */
class JobListingEntityDeleteForm extends ContentEntityDeleteForm {


}
