<?php

namespace Drupal\joblistings\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Job Listing entities.
 *
 * @ingroup joblistings
 */
interface JobListingEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Job Listing name.
   *
   * @return string
   *   Name of the Job Listing.
   */
  public function getName();

  /**
   * Sets the Job Listing name.
   *
   * @param string $name
   *   The Job Listing name.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity.
   */
  public function setName($name);

  /**
   * Gets the Job Listing creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Job Listing.
   */
  public function getCreatedTime();

  /**
   * Sets the Job Listing creation timestamp.
   *
   * @param int $timestamp
   *   The Job Listing creation timestamp.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Job Listing revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Job Listing revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Job Listing revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Job Listing revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity.
   */
  public function setRevisionUserId($uid);


  /**
   * Custom getters and setters for the Job Listing interface.
   */

  /**
   * Gets the crelate ID of the Job Listing entity.
   *
   * @return string
   *   ID string of the Job Listing entity.
   */
  public function getCrelateId();

  /**
   * Gets the status of the Job Listing entity.
   *
   * @return boolean
   *   The status of the Job Listing entity.
   */
  public function getJobStatus();

  /**
   * Sets the Job Status of the Job Listing entity.
   *
   * @param boolean $job_status
   * The Job Listing entity status flag.
   *
   * @return boolean
   * The status flag of the entity.
   */
  public function setJobStatus($job_status);

  /**
   * Gets the status of the Job Listing entity.
   *
   * @return boolean
   *   The status of the Job Listing entity.
   */
  public function getIsJobFeatured();

  /**
   * Sets the Job Status of the Job Listing entity.
   *
   * @param bool $is_job_featured
   * The Job Listing entity status flag.
   *
   * @return boolean
   * The status flag of the entity.
   */
  public function setIsJobFeatured($is_job_featured);

  /**
   * Gets the modified timestamp of the Job Listing entity.
   *
   * @return int
   *   The UNIX timestamp of when this Job Listing was last modified.
   */
  public function getModifiedDate();

  /**
   * Sets the modified timestamp of the Job Listing entity.
   *
   * @param int $timestamp
   * The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity.
   */
  public function setModifiedDate($timestamp);

  /**
   * Gets the posted timestamp of the Job Listing entity.
   *
   * @return int
   *   The UNIX timestamp of when this Job Listing was last modified.
   */
  public function getPostedDate();

  /**
   * Sets the posted timestamp of the Job Listing entity.
   *
   * @param int $timestamp
   * The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity.
   */
  public function setPostedDate($timestamp);

  /**
   * Gets the description of the Job Listing entity.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   * The Job Listing entity.
   */
  public function getJobDescription();

  /**
   * Sets the description of the Job Listing entity.
   *
   * @param string $description
   * The Job Listing entity description.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity entity.
   */
  public function setJobDescription($description);

  /**
   * Gets the description of the Job Listing entity.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   * The Job Listing entity.
   */
  public function getJobLocationState();

  /**
   * Sets the description of the Job Listing entity.
   *
   * @param string $state
   * The Job Listing entity description.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity entity.
   */
  public function setJobLocationState($state);

  /**
   * Gets the description of the Job Listing entity.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   * The Job Listing entity.
   */
  public function getJobLocationCity();

  /**
   * Sets the description of the Job Listing entity.
   *
   * @param string $city
   * The Job Listing entity description.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity entity.
   */
  public function setJobLocationCity($city);

  /**
   * Gets the description of the Job Listing entity.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   * The Job Listing entity.
   */
  public function getJobCompensation();

  /**
   * Sets the description of the Job Listing entity.
   *
   * @param string $compensation
   * The Job Listing entity description.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   *   The called Job Listing entity entity.
   */
  public function setJobCompensation($compensation);

  /**
   * Gets the Crelate Job Code identifier for the Job Listing entity.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   * The Job Listing entity.
   */
  public function getJobCode();

  /**
   * Gets the Crelate Job Owner for the Job Listing entity.
   *
   * @return \Drupal\joblistings\Entity\JobListingEntityInterface
   * The Job Listing entity.
   */
  public function getJobOwner();

  /**
   * Gets the tags of the Job Listing entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   * An array of Entities for the taxonomy term(s).
   */
  public function getJobTags();

  /**
   * Sets the tech stack of the Account entity.
   *
   * @param TermInterface $job_tags
   * The vocabulary ID of the taxonomy term.
   *
   * @return \Drupal\accounts\Entity\AccountEntityInterface
   *   The Job Listing entity.
   */
  public function setJobTags($job_tags);

}
